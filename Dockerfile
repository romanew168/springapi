# Указываем базовый образ с установленной Java 17
FROM amazoncorretto:17

# Копируем скомпилированные файлы приложения в контейнер
COPY build/libs/myapp.jar /app/myapp.jar

# Устанавливаем рабочую директорию
WORKDIR /app

# Открываем порт, если это необходимо
EXPOSE 8080

# Запускаем приложение
CMD ["java", "-jar", "myapp.jar"]